@extends('layouts.app_loggedout')

@section('content')
<div class="register" style="">
    <div class="uk-flex uk-flex-middle uk-flex-center" style="min-height: 100vh;">
        <div style="max-width: 100%; width: 450px;">
            <h1>Log In</h1>

            <form class="uk-form-stacked" action="{{ route('login') }}" method="POST" action autocomplete="on" style="text-align: center;">
                @csrf

                <div class="uk-margin">
                    <div class="uk-form-controls">
                        <input placeholder="eMail" class="uk-input" value="{{ old('email') }}" name="email" required>
                    </div>
                    @if ($errors->has('email'))
                        <div class="formerror" role="alert" style="text-align: left;">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @endif                    
                </div>

                <div class="uk-margin">
                    <div class="uk-form-controls">
                        <input placeholder="Password" type="password" class="uk-input" name="password" required>
                    </div>
                    @if ($errors->has('password'))
                        <div class="formerror" role="alert" style="text-align: left;">
                            <strong>{{ $errors->first('password') }}</strong>
                        </div>
                    @endif                    
                </div> 

                <div class="uk-margin uk-flex uk-flex-right">
                    <button type="submit" class="uk-width-1-1 uk-button uk-button-primary" style="border-radius: 0px !important;">Log In</button>
                </div>

                <div>
                    You don't have an account yet? Then sign up <a href="{{ route('register') }}" class="uk-link">here</a>.
                </div>
            </form>
        </div>
    </div>        
</div>
@endsection
