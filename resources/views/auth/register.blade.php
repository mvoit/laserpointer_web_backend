@extends('layouts.app_loggedout')

@section('content')
<div class="register" style="">
    <div class="uk-flex uk-flex-middle uk-flex-center" style="min-height: 100vh;">
        <div style="max-width: 100%; width: 450px;">
            <h1>Sign Up</h1>

            <form class="uk-form-stacked" action="{{ route('register') }}" method="POST" action autocomplete="off" style="text-align: center;">
                @csrf

                <div class="uk-margin">
                    <div class="uk-form-controls">
                        <input placeholder="Your name" class="uk-input" value="{{ old('name') }}" name="name" required autofocus>
                    </div>
                    @if ($errors->has('name'))
                        <div class="formerror" role="alert" style="text-align: left;">
                            <strong>{{ $errors->first('name') }}</strong>
                        </div>
                    @endif            
                </div>

                <div class="uk-margin">
                    <div class="uk-form-controls">
                        <input placeholder="eMail" class="uk-input" value="{{ old('email') }}" name="email" required>
                    </div>
                    @if ($errors->has('email'))
                        <div class="formerror" role="alert" style="text-align: left;">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @endif                    
                </div>

                <div class="uk-margin">
                    <div class="uk-form-controls">
                        <input placeholder="Password" type="password" class="uk-input" name="password" required>
                    </div>
                    @if ($errors->has('password'))
                        <div class="formerror" role="alert" style="text-align: left;">
                            <strong>{{ $errors->first('password') }}</strong>
                        </div>
                    @endif                    
                </div>

                <div class="uk-margin">
                    <div class="uk-form-controls">
                        <input placeholder="Repeat your password" type="password" class="uk-input" name="password_confirmation" required>
                    </div>
                    @if ($errors->has('password_confirmation'))
                        <div class="formerror" role="alert" style="text-align: left;">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </div>
                    @endif                    
                </div>               

                <div class="uk-margin uk-flex uk-flex-right">
                    <button type="submit" class="uk-width-1-1 uk-button uk-button-primary" style="border-radius: 0px !important;">Register</button>
                </div>

                <div>
                    You already have an account? Then sign in <a href="{{ route('login') }}" class="uk-link">here</a>.
                </div>
            </form>
        </div>
    </div>        
</div>
@endsection
