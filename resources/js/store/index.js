import Vue from 'vue';
import Vuex from 'vuex';
import defecttypes from './modules/defecttypes.js';
import defects from './modules/defects.js';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

const actions = { dummy: { handler: () => {} } };

export default new Vuex.Store({
    actions,
    modules: {
        defecttypes,
        defects
    },
    strict: debug,
});
