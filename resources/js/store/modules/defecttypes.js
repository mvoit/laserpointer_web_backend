// initial state
const state = {
    defecttypes: [],
    defecttypes_loaded: false
}

// getters
const getters = {
    defecttypeById: (state) => (id) => {
        return state.defecttypes.find(dt => dt.id === id);
    }
}

// actions
const actions = {

    loadDefectTypes({commit}) {
        return new Promise((resolve, reject) => {
            axios.get("/defecttypes")
            .then(function (response) {
                // console.log("defecttypes: " + JSON.stringify(response.data));
                commit("SET_DEFECTTYPES", response.data.data);
                return resolve(response);
            })
            .catch(function (error) {
                return reject(error);
            });
        });
    },
}
  
// mutations
const mutations = {
    SET_DEFECTTYPES(state, defecttypes) {
        state.defecttypes = defecttypes;
        state.defecttypes_loaded = true;
    },

    UPDATE_DEFECTTYPES(state, defecttypes) {
        state.defecttypes = defecttypes;
    }
}
  
export default {
    state,
    getters,
    actions,
    mutations
}
