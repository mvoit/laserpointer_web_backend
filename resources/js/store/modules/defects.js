// initial state
const state = {
    defects: [],
}

// getters
const getters = {

}

// actions
const actions = {

    loadDefects({commit}) {
        return new Promise((resolve, reject) => {
            axios.get("/defects")
            .then(function (response) {
                commit("SET_DEFECTS", response.data);
                return resolve(response);
            })
            .catch(function (error) {
                return reject(error);
            });
        });
    },
}
  
// mutations
const mutations = {
    SET_DEFECTS(state, defects) {
        state.defects = defects;
    },

    ADD_DEFECT(state, defect) {
        state.defects = [...state.defects, defect];
    }
}
  
export default {
    state,
    getters,
    actions,
    mutations
}
