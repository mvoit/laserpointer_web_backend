import LayoutDefaultLoggedIn from './components/LayoutDefaultLoggedIn.vue';
import Home from './components/Home.vue';

export default [
    {
        path: '/',
        component: LayoutDefaultLoggedIn,
        children: [
            {name: 'Home', path: '', component: Home}
        ],        
        meta: { }
    },
];
