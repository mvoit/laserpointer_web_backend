#!/usr/bin/env bash

set -e

(cd /var/www/html && php artisan config:clear && php artisan config:cache && php artisan route:clear && php artisan view:clear)

# echo "Migrating"
# (cd /var/www/html && php artisan migrate --force)

ln -sf /etc/supervisor/conf.d-available/app.conf /etc/supervisor/conf.d/app.conf

exec supervisord -c /etc/supervisor/supervisord.conf
