#!/usr/bin/env bash

set -e

role=${CONTAINER_ROLE:-app}
env=${APP_ENV:-production}

echo "Copying .env"
(cd /var/www/html && cp deploy_configs/.env.${env} ./.env)

echo "Copying auth keys"
(cd /var/www/html && cp deploy_configs/storage/oauth-private.key storage && cp deploy_configs/storage/oauth-public.key storage)

if [ "$env" != "local" ]; then
    echo "Caching configuration..."
    (cd /var/www/html && php artisan config:clear && php artisan config:cache && php artisan route:clear && php artisan view:clear)

    # echo "Migrating"
    # (cd /var/www/html && php artisan migrate --force)

    echo "Removing Xdebug..."
    rm -rf /usr/local/etc/php/conf.d/{docker-php-ext-xdebug,xdebug}.ini
fi


# App
if [ "$role" = "app" ]; then
    
    ln -sf /etc/supervisor/conf.d-available/app.conf /etc/supervisor/conf.d/app.conf

# Queue
elif [ "$role" = "queue" ]; then
    
    ln -sf /etc/supervisor/conf.d-available/queue.conf /etc/supervisor/conf.d/queue.conf

# Scheduler
elif [ "$role" = "scheduler" ]; then

    ln -sf /etc/supervisor/conf.d-available/scheduler.conf /etc/supervisor/conf.d/scheduler.conf

else
    echo "Could not match the container role \"$role\""
    exit 1
fi

exec supervisord -c /etc/supervisor/supervisord.conf