#!/usr/bin/env bash

(sudo docker-compose exec app php artisan config:clear && sudo docker-compose exec app php artisan config:cache)
(sudo docker-compose exec app php artisan route:clear && sudo docker-compose exec app php artisan view:clear)
(sudo docker-compose exec app php artisan migrate --force)
