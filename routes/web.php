<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

// Route::get('/settings', 'SettingsController@index')->name('settings');

Route::resource('/defecttypes', 'DefecttypeController');
Route::post('/defecttypes/updateOrder', 'DefecttypeController@updateOrder');
Route::resource('/customremotesettings', 'CustomRemoteSettingsController');

Route::resource('/defects', 'DefectController');
Route::post('/defects/clear', 'DefectController@clear');
Route::post('/defects/send', 'DefectController@send');
Route::resource('/users', 'UserController');

Route::post('/info', 'InfoController@index');
