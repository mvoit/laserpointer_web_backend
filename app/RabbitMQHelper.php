<?php

namespace App;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQHelper
{
    public static function send($event, $payload) {
        $rabbitmq_host = config('queue.connections.rabbitmq.host');
        $rabbitmq_port = config('queue.connections.rabbitmq.port');
        $rabbitmq_exchange = config('queue.connections.rabbitmq.exchange');
        $rabbitmq_login = config('queue.connections.rabbitmq.login');
        $rabbitmq_password = config('queue.connections.rabbitmq.password');

        \Log::info("RabbitMQ host: " . $rabbitmq_host);
        \Log::info("RabbitMQ port: " . $rabbitmq_port);
        \Log::info("RabbitMQ exchange name: " . $rabbitmq_exchange);

        $connection = new AMQPStreamConnection($rabbitmq_host, $rabbitmq_port, $rabbitmq_login, $rabbitmq_password);
        $channel = $connection->channel();
        // $channel->queue_declare('default', false, false, false, false);
        $data = [
            'event' => $event,
            'payload' => $payload
        ];
        $msg = new AMQPMessage( json_encode($data) );
        $channel->basic_publish($msg, $rabbitmq_exchange);                        
    }
}
