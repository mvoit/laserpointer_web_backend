<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class RemoteChange implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $custom_remote_settings = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($custom_remote_settings)
    {
        $this->custom_remote_settings = $custom_remote_settings;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::info("RemoteChange::handle");
        echo "RemoteChange::handle" . PHP_EOL;
    }
}
