<?php

namespace App\Listeners;

use App\Events\NewDefect;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNewDefectNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewDefect  $event
     * @return void
     */
    public function handle(NewDefect $event)
    {
        //
    }
}
