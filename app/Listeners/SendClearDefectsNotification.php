<?php

namespace App\Listeners;

use App\Events\ClearDefects;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendClearDefectsNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ClearDefects  $event
     * @return void
     */
    public function handle(ClearDefects $event)
    {
        //
    }
}
