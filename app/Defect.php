<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Defect extends Model
{
    protected $fillable = ['x', 'y', 'z', 'defecttype_id'];

    protected $appends = [];

    public function defecttype() {
        return $this->belongsTo(App\Defecttype::class);
    }
}
