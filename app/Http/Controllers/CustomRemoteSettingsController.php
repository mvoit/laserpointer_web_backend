<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Defecttype;
use App\Defect;
use App\CustomRemoteSetting;
use App\Jobs\RemoteChange;
use App\RabbitMQHelper;

class CustomRemoteSettingsController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $remoteSettings = CustomRemoteSetting::firstOrCreate([
            'user_id' => Auth::user()->id
        ],[
            'buttonmapping' => json_encode([1, 2, 3, 4, 5, 6, 7])
        ]);

        return $remoteSettings;
    }

    public function store(Request $request) {
        \Log::info("input: " . json_encode($request->all()));

        $buttonmapping = json_decode($request->input("buttonmapping", []), true);
        $model = CustomRemoteSetting::updateOrCreate([
            'user_id' => Auth::user()->id
        ], [
            'buttonmapping' => json_encode($buttonmapping)
        ]);            

        $remotesettings = CustomRemoteSetting::getSettingsForUser(Auth::user());
        $defecttypes = Defecttype::orderBy('order_id', 'asc')->get();
        $defects = Defect::all();
        $data = [
            'remotemapping' => $remotesettings,
            'defecttypes' => $defecttypes,
            'defects' => $defects
        ];

        try {
            RabbitMQHelper::send( 'data', $data );
        } catch (\ErrorException $e) {
            \Log::error("RabbitMQ not working");
        }
    }

}
