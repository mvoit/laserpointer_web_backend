<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Defect;
use App\Jobs\DChange;
use App\Events\NewDefect;
use App\Events\ClearDefects;
use App\RabbitMQHelper;
use App\CustomRemoteSetting;
use App\Defecttype;

class DefectController extends Controller
{
    public function __construct() {
        $this->middleware('auth', ['except' => ['store', 'clear']]);
    }

    public function index() {
        return Defect::all();
    }

    public function store(Request $request) {
        \Log::info("DefectController::store: " . json_encode($request->all()));
        $model = Defect::create($request->all());
        event(new NewDefect( $model ));
    }

    public function clear(Request $request) {
        Defect::truncate();
        $defects = Defect::all();
        $data = [
            'defects' => $defects
        ];

        try {
            RabbitMQHelper::send( 'data', $data );
        } catch (\ErrorException $e) {
            \Log::error("RabbitMQ not working");
        }
        event(new ClearDefects());
    }

    public function send(Request $request) {
        $defect = Defect::create([
            "x" => 65,
            "y" => 35,
            "z" => 0,
            "defecttype_id" => 2
        ]);

        event(new NewDefect( $defect ));        
    }
}
