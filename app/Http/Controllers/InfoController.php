<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Defecttype;
use App\Defect;
use App\CustomRemoteSetting;
use App\RabbitMQHelper;

class InfoController extends Controller
{
    public function __construct() {
    }

    public function index() {
        $remotesettings = CustomRemoteSetting::getSettingsForUser(Auth::user());
        $defecttypes = Defecttype::orderBy('order_id', 'asc')->get();
        $defects = Defect::all();
        $data = [
            'remotemapping' => $remotesettings,
            'defecttypes' => $defecttypes,
            'defects' => $defects
        ];

        try {
            RabbitMQHelper::send( 'data', $data );
        } catch (\ErrorException $e) {
            \Log::error("RabbitMQ not working");
        }

        return response()->json([
            'remote' => $remotesettings,
            'defecttypes' => $defecttypes,
            'defects' => $defects
        ]);
    }
}
