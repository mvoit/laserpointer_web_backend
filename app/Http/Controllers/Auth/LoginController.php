<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\CustomRemoteSetting;
use Illuminate\Http\Request;
use App\Jobs\RemoteChange;
use App\RabbitMQHelper;
use App\Defecttype;
use App\Defect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user) {
        
        $remotesettings = CustomRemoteSetting::getSettingsForUser(Auth::user());
        $defecttypes = Defecttype::orderBy('order_id', 'asc')->get();
        $defects = Defect::all();
        $data = [
            'remotemapping' => $remotesettings,
            'defecttypes' => $defecttypes,
            'defects' => $defects
        ];

        try {
            RabbitMQHelper::send( 'data', $data );
        } catch (\ErrorException $e) {
            \Log::error("RabbitMQ not working");
        }

        return redirect()->route('home');
    }
}
