<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Defecttype;
use App\Defect;
use App\CustomRemoteSetting;
use App\Jobs\DTChange;
use App\Http\Resources\DefecttypeResource;
use App\RabbitMQHelper;

class DefecttypeController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $defecttypes = Defecttype::orderBy('order_id', 'asc')->get();
        return DefecttypeResource::collection($defecttypes);
    }

    public function store(Request $request) {
        $filenameUpload =  $request->file('file')->store("public/defect_icons");
        $model = Defecttype::create([
            'name' => $request->name,
            'filename_icon' => $filenameUpload,
            'order_id' => Defecttype::count()
        ]);

        $remotesettings = CustomRemoteSetting::getSettingsForUser(Auth::user());
        $defecttypes = Defecttype::orderBy('order_id', 'asc')->get();
        $defects = Defect::all();
        $data = [
            'remotemapping' => $remotesettings,
            'defecttypes' => $defecttypes,
            'defects' => $defects
        ];

        try {
            RabbitMQHelper::send( 'data', $data );
        } catch (\ErrorException $e) {
            \Log::error("RabbitMQ not working");
        }

        return $model;
    }

    public function update(Request $request, Defecttype $defecttype) {
        \Log::info("request: " . json_encode($request->all()));
        $original_filename = $request->file->getClientOriginalName();
        \Log::info("filename: " . $original_filename);

        if ($request->hasFile('file')) {
            $filenameUpload =  $request->file('file')->storeAs("public/defect_icons", $original_filename);
            $defecttype->update(array_merge($request->all(), [
                'filename_icon' => $filenameUpload,
            ]));
        }
        else {
            $defecttype->update($request->all());
        }

        RabbitMQHelper::send( 'defecttypes', Defecttype::orderBy('order_id', 'asc')->get() );

        return $defecttype;
    }

    public function destroy(Defecttype $defecttype) {
        $defecttype->delete();
    }

    public function updateOrder(Request $request) {
        $defecttypes = json_decode($request->input("defecttypes", []), true);

        $newOrder = [];
        foreach($defecttypes as $index => $defecttype) {
            $defecttype = Defecttype::findorFail( $defecttype['id'] );
            $defecttype->order_id = $index;
            $defecttype->save();
        }

        RabbitMQHelper::send( 'defecttypes', Defecttype::orderBy('order_id', 'asc')->get() );
    }

}
