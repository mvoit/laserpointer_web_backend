<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class CustomRemoteSetting extends Model
{
    protected $fillable = ['user_id', 'buttonmapping'];

    public $timestamps = false;

    public static function getSettingsForUser($user) {
        return CustomRemoteSetting::firstOrCreate([
            'user_id' => $user->id
        ], [
            'buttonmapping' => json_encode( [1, 2, 3, 4, 5, 6, 7] )
        ]);
    }
}
