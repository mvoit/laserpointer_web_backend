<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Defecttype extends Model
{
    protected $fillable = ['name', 'filename_icon', 'order_id'];

    protected $appends = ['iconUrl'];

    public function getIconUrlAttribute() {
        if (!$this->filename_icon) return null;
        return Storage::disk('local')->url($this->filename_icon);
    }
}
