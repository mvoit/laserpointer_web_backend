#!/usr/bin/env bash

(sudo docker-compose stop)
(sudo docker-compose up -d && sudo docker-compose exec app php artisan config:clear && sudo docker-compose exec app php artisan route:clear && sudo docker-compose exec app php artisan view:clear)
